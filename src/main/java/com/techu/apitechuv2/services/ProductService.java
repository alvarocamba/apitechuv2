package com.techu.apitechuv2.services;

import com.techu.apitechuv2.models.ProductModel;
import com.techu.apitechuv2.repositories.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductService {

    @Autowired
    ProductRepository productRepository;

    public List<ProductModel> findAll() {
        return  this.productRepository.findAll();
    }

    public Optional<ProductModel> findById(String id){ //el optinal es porque puede ser que exista o no el valor en la bbdd, no es obligaria la existencia
        System.out.println("findById");
        System.out.println("Obteniendo el producto con la id " + id);

        return this.productRepository.findById(id);
    }

    public ProductModel add(ProductModel product){
        System.out.println("add");

        return this.productRepository.save(product); //save inserta y actualiza. Si no queremos actualización, utilizaremos insert.
    }

    // Ini: Mi código
    public ProductModel saveById(ProductModel product){
        System.out.println("saveById");
        System.out.println("Actualizando el producto con la id " + product.getId());

        return this.productRepository.save(product);
    }
    // Fin: Mi código

    // Ini: Código Carlos
    public ProductModel update(ProductModel product){
        System.out.println("update");
        System.out.println("Actualizando el producto con la id " + product.getId());

        return this.productRepository.save(product);
    }
    // Fin: Código Carlos

    // Ini: Mi código
    public Optional<ProductModel> deleteById(String id){
        System.out.println("deleteById");
        System.out.println("Eliminando el producto con la id " + id);

        Optional<ProductModel> result = this.productRepository.findById(id);

        this.productRepository.deleteById(id);

        return result;
    }
    // Fin: Mi código

    // Ini: Código Carlos
    public boolean delete(String id){
        System.out.println("delete");
        boolean result = false;

        if (this.productRepository.findById(id).isPresent() == true){
            System.out.println("Producto encontrado, borrado");
            this.productRepository.deleteById(id);
            result = true;
        };

        return result;
    }
    // Fin: Código Carlos

}
